#include "widget.h"
#include "ui_widget.h"

#include <QLabel>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    QLabel* label = new QLabel(this);
    label->setText("This is a paragraph of text");

    QFont font;//创建字体
    font.setFamily("仿宋");//字体家族
    font.setPixelSize(30);//字体大小
    font.setBold(true);//字体加粗
    font.setItalic(true);//字体加粗
    font.setUnderline(true);//字体下划线
    font.setStrikeOut(true);//字体删除线

    label->setFont(font);//把font对象放进label中
}

Widget::~Widget()
{
    delete ui;
}

