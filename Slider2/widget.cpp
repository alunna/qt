#include "widget.h"
#include "ui_widget.h"
#include <QShortcut>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //增加快捷键
    //需要使用QShortCut类
    QShortcut* shortCut1 = new QShortcut(this);
    shortCut1->setKey(QKeySequence("-"));
    QShortcut* shortCut2 = new QShortcut(this);
    shortCut2->setKey(QKeySequence("="));

    //连接-和+到快捷键
    connect(shortCut1,&QShortcut::activated,this,&Widget::subValue);
    connect(shortCut2,&QShortcut::activated,this,&Widget::addValue);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_horizontalSlider_valueChanged(int value)
{
    //将值显示到label上
    ui->label->setText("当前值为："+ QString::number(value));
}

void Widget::addValue()
{
    //如果不大于最大值,就可以++
    int value = ui->horizontalSlider->value();
    if(value >= ui->horizontalSlider->maximum())
    {
        return;
    }
    ui->horizontalSlider->setValue(value + 5);

}

void Widget::subValue()
{
    int value = ui->horizontalSlider->value();
    if(value <= ui->horizontalSlider->minimum())
    {
        return;
    }
    ui->horizontalSlider->setValue(value - 5);
}
