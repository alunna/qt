#include "widget.h"
#include "ui_widget.h"

#include <QLabel>
#include <QDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //先再每个标签页中创建label
    QLabel* label1 = new QLabel(ui->tab);
    label1->setText("标签页1");
    label1->resize(100,50);

    QLabel* label2 = new QLabel(ui->tab_2);
    label2->setText("标签页2");
    label2->resize(100,50);


}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    //使用addtab 来实现创建新的标签页
    //参数1 指定一个QWidget
    //参数2 指定标签页的Text
    int count = ui->tabWidget->count();//找到标签页数量
    QWidget* w = new QWidget();
    ui->tabWidget->addTab(w,QString("Tab ")+QString::number(count+1));
    //添加label
    QLabel* label = new QLabel(w);
    label->setText(QString("标签页")+QString::number(count +1));
    label->resize(100,50);

    //设置新的标签页被设置
    ui->tabWidget->setCurrentIndex(count);
}

void Widget::on_pushButton_2_clicked()
{
    //先获取当前页的下标
    int index = ui->tabWidget->currentIndex();
    //删除标签页
    ui->tabWidget->removeTab(index);
}

void Widget::on_tabWidget_currentChanged(int index)
{
    qDebug()<<"当前的页数是"<<index;
}
