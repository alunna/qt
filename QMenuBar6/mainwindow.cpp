#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

//    QMenuBar* menuBar = new QMenuBar();
    QMenuBar* menuBar = this->menuBar();
    this->setMenuBar(menuBar);

    QMenu* menu1 = new QMenu("父菜单");
    QMenu* menu2 = new QMenu("子菜单");
    menuBar->addMenu(menu1);
    menu1->addMenu(menu2);

    menu2->setIcon(QIcon(":/save_file.png"));

    QAction* action1 = new QAction("open file");
    action1->setIcon(QIcon(":/open_file.png"));
    QAction* action2 = new QAction("save file");
    action2->setIcon(QIcon(":/save_file.png"));
    menu1->addAction(action1);
    menu1->addAction(action2);
}

MainWindow::~MainWindow()
{
    delete ui;
}

