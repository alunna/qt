#include "widget.h"
#include "ui_widget.h"
#include <QtDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //初始话下拉菜单
    //ui->comboBox->addItem("鸡腿堡");
    //针对spinBox进行设置范围
    ui->spinBox->setRange(1,5);
    ui->spinBox_2->setRange(1,5);
    ui->spinBox_3->setRange(1,5);

    ui->spinBox->setValue(1);
    ui->spinBox_2->setValue(1);
    ui->spinBox_3->setValue(1);

}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    qDebug()<<"当前下单的内容是:"
           <<ui->comboBox->currentText()<<":"<<ui->spinBox->value()
          <<ui->comboBox_2->currentText()<<":"<<ui->spinBox_2->value()
         <<ui->comboBox_3->currentText()<<":"<<ui->spinBox_3->value();
}
