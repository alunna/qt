#include "widget.h"
#include "ui_widget.h"
#include <QtDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_textEdit_textChanged()
{
    qDebug()<<"textChanged"<<ui->textEdit->toPlainText();
}

void Widget::on_textEdit_selectionChanged()
{
    QTextCursor cursor = ui->textEdit->textCursor();//可以描述光标选择内容的情况
    qDebug()<<"selectChange"<<cursor.selectedText();
}

void Widget::on_textEdit_cursorPositionChanged()
{
    QTextCursor cursor = ui->textEdit->textCursor();//光标的位置情况
    qDebug()<<"CursonPosition"<<cursor.position();
}

void Widget::on_textEdit_undoAvailable(bool b)
{

     qDebug()<<"undoAvailable"<<b;//撤销Ctrl+Z
}

void Widget::on_textEdit_redoAvailable(bool b)
{
    qDebug()<<"redoAvailable"<<b;//撤销撤销CTRL+y
}

void Widget::on_textEdit_copyAvailable(bool b)
{
    qDebug()<<"copyAvailable"<<b;
}
