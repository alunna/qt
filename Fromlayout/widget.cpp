#include "widget.h"
#include "ui_widget.h"

#include <QFormLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //设置成三行两列
    QFormLayout* layout = new QFormLayout();
    this->setLayout(layout);

    //创建三个label作为第一列
    QLabel* label1 = new QLabel("姓名");
    QLabel* label2 = new QLabel("年龄");
    QLabel* label3 = new QLabel("电话");

    //创建三个lineEdit作为第二列
    QLineEdit* line1 = new QLineEdit();
    QLineEdit* line2 = new QLineEdit();
    QLineEdit* line3 = new QLineEdit();

    //上述表单添加到窗口中
    layout->addRow(label1,line1);
    layout->addRow(label2,line2);
    layout->addRow(label3,line3);

    //创建一个提交按钮
    QPushButton* button = new QPushButton("提交");
    layout->addRow(nullptr,button);

}

Widget::~Widget()
{
    delete ui;
}

