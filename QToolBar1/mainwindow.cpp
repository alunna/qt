#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QToolBar>
#include <QDebug>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //创建工具栏
    QToolBar* toolBar = new QToolBar();
    this->addToolBar(toolBar);

    //创建菜单栏
    QMenuBar* menuBar = new QMenuBar();
    this->setMenuBar(menuBar);

    //创建菜单
    QMenu* menu = new QMenu("文件");
    menuBar->addMenu(menu);

    //创建功能信息键
    QAction* action1 = new QAction("保存");
    QAction* action2 = new QAction("打开");


    //设置提示信息
    action1->setToolTip("点击保存文件");
    action2->setToolTip("点击打开文件");

    //插入图标
    action1->setIcon(QIcon(":/save_file.png"));
    action2->setIcon(QIcon(":/open_file.png"));

    //将功能键放到菜单中
    menu->addAction(action1);
    menu->addAction(action2);

    //将功能键放到工具栏中
    toolBar->addAction(action1);
    toolBar->addAction(action2);

    connect(action1,&QAction::triggered,this,&MainWindow::handle1);
    connect(action2,&QAction::triggered,this,&MainWindow::handle2);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handle1()
{
    qDebug() << "文件已保存！";
}

void MainWindow::handle2()
{
    qDebug() << "文件已打开！";
}

