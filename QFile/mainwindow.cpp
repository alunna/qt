#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPlainTextEdit>

#include <QFileDialog>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle("简单的记事本");
    // 获取到菜单栏
    //有的话直接获取，没有的话进行创建
    QMenuBar* menuBar = this->menuBar();

    //添加菜单
    QMenu* menu = new QMenu("文件");
    menuBar->addMenu(menu);

    //添加菜单项
    QAction* action1 = new QAction("打开");
    QAction* action2 = new QAction("保存");
    menu->addAction(action1);
    menu->addAction(action2);

    //指定一个输入框
    edit = new QPlainTextEdit();
    QFont font;
    font.setPixelSize(20);
    edit->setFont(font);
    this->setCentralWidget(edit);

    //连接QAction的信号槽
    connect(action1,&QAction::triggered,this,&MainWindow::handleAction1);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handleAction1()
{
    //先弹出一个打开文件的文件筐
    QString path = QFileDialog::getOpenFileName(this);

    //把文件放到状态栏中
    QStatusBar* statusBar = this->statusBar();
    statusBar->showMessage(path);

    //根据用户选择的路径构建一个QFile对象,并打开文件
    QFile file(path);
    bool isOpen = file.open(QIODevice::ReadOnly);
    if(isOpen == false)
    {
        statusBar->showMessage(path + "打开失败！！");
        return;
    }

    //读取文件
    QString text = file.readAll();//确保这个文件是个文本文件，如果是二进制文件(像文本文件、执行程序、图片、音频)交给 QString 就不合适了

    //关闭文件
    file.close();

    //读到的内容设置到输入框中
    edit->setPlainText(text);

}

void MainWindow::handleAction2()
{
    //弹出一个保存文件的文件筐
    QString path = QFileDialog::getOpenFileName(this);

    //在状态栏中显示文件名
    QStatusBar* statusBar = this->statusBar();
    statusBar->showMessage(path);

    //根据路径构造 QFile 对象，打开文件
    QFile file;
    bool isOpen = file.open(QFile::WriteOnly);
    if(isOpen == false)
    {
        statusBar->showMessage(path + "文件打开失败");
        return;
    }

    //写文件
    const QString& text = edit->toPlainText();
    file.write(text.toUtf8());

    //关闭文件
    file.close();
}

