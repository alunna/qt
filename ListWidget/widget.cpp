#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //往这里添加一些元素
//    ui->listWidget->addItem("C++");
//    ui->listWidget->addItem("Java");
//    ui->listWidget->addItem("ptyhon");

    ui->listWidget->addItem(new QListWidgetItem("C++"));
    ui->listWidget->addItem(new QListWidgetItem("Java"));
    ui->listWidget->addItem(new QListWidgetItem("Python"));
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_insert_clicked()
{
    //1、获取到输入框中的内容
    const QString text = ui->lineEdit->text();
    //2、添加到列表中
    ui->listWidget->addItem(text);
}

void Widget::on_pushButton_delete_clicked()
{
    //获取到被选中的元素
    int row = ui->listWidget->currentRow();
    if(row<0)
    {
        return;
    }
    //删除元素
    ui->listWidget->takeItem(row);
}

void Widget::on_listWidget_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    //通过该参数感知到变化
    if(current!=nullptr)
    {
        qDebug()<<"当前选中的元素为："<<current->text();
    }
    if(previous!=nullptr)
    {
        qDebug()<<"上次选中的元素为："<<previous->text();
    }
}
