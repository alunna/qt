#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //使用时间戳来获得随机种子
    srand(time(0));
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_agree_clicked()
{
    ui->label->setText("kiss");
}

void Widget::on_pushButton_reject_clicked()
{
    //
}

void Widget::on_pushButton_reject_pressed()
{
    //点了拒绝，按钮挪走
    //通过随机数，随机挪走

    int width = this->geometry().width();
    int height = this->geometry().height();
    int x = rand() % width;
    int y = rand() % height;

    ui->pushButton_reject->move(x,y);
}
