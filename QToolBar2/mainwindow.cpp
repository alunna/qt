#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QToolBar>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //创建多个工具栏
    QToolBar* toolBar1 = new QToolBar();
    QToolBar* toolBar2 = new QToolBar();

    //方法一：创建工具栏的同时指定出现位置
    this->addToolBar(toolBar1);
    this->addToolBar(Qt::RightToolBarArea,toolBar2);

    //方法二：使⽤ QToolBar类 提供的 setAllowedAreas()函数 设置停靠位置。
    //toolBar1->setAllowedAreas(Qt::LeftToolBarArea);//只允许存在左边或者浮动
    toolBar1->setAllowedAreas(Qt::LeftToolBarArea | Qt::RightToolBarArea);//只允许存在左边和右边和浮动
    //设置不允许浮动
    toolBar1->setFloatable(false);

    //设置不允许移动
    toolBar2->setMovable(false);
    QAction* action1 = new QAction("动作1");
    QAction* action2 = new QAction("动作2");
    QAction* action3 = new QAction("动作3");
    QAction* action4 = new QAction("动作4");
    toolBar1->addAction(action1);
    toolBar1->addAction(action2);
    toolBar2->addAction(action3);
    toolBar2->addAction(action4);


}

MainWindow::~MainWindow()
{
    delete ui;
}

