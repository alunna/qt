#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QLabel>
#include <QProgressBar>
#include <QPushButton>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //存在就获取，不存在就创建
    QStatusBar* statusBar = this->statusBar();
    //保证能够可以获取完好的状态栏
    this->setStatusBar(statusBar);

    //显示一个临时的信息
    //statusBar->showMessage("这是一个状态栏消息！",3000);//显示3000ms，可以指定时间，timeout不填，消息会一直存在

    //给状态栏中添加一个子控件
    QLabel* label1 = new QLabel("这是一个Label1");
    statusBar->addWidget(label1);//可以设置拉伸系数

    //设置进度条
    QProgressBar* prog = new QProgressBar();
    prog->setRange(0,100);
    prog->setValue(30);
    statusBar->addWidget(prog);

    QPushButton* button = new QPushButton();
    button->setText("按钮");
    statusBar->addPermanentWidget(button);//从右往左放置
}

MainWindow::~MainWindow()
{
    delete ui;
}

