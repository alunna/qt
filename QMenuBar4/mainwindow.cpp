#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QMenuBar* menuBar = new QMenuBar();
    this->setMenuBar(menuBar);

    QMenu* menuParent = new QMenu("父菜单");
    QMenu* menuChild = new QMenu("子菜单");
    //把子菜单添加到父菜单中
    menuBar->addMenu(menuParent);
    menuParent->addMenu(menuChild);
    //给子菜单添加菜单项
    QAction* action1 = new QAction("菜单项1");
    QAction* action2 = new QAction("菜单项2");
    menuChild->addAction(action1);
    menuChild->addAction(action2);
    //给父菜单添加菜单项
    QAction* action3 = new QAction("菜单项3");
    menuParent->addAction(action3);
}

MainWindow::~MainWindow()
{
    delete ui;
}

