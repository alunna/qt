#include "widget.h"
#include "ui_widget.h"
#include <QPushButton>

#include <QGridLayout>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    QPushButton* button1 = new QPushButton("按钮1");
    QPushButton* button2 = new QPushButton("按钮2");
    QPushButton* button3 = new QPushButton("按钮3");
    QPushButton* button4 = new QPushButton("按钮4");
    QPushButton* button5 = new QPushButton("按钮5");
    QPushButton* button6 = new QPushButton("按钮6");

    QGridLayout* layout = new QGridLayout();
    layout->addWidget(button1,0,0);
    layout->addWidget(button2,0,1);
    layout->addWidget(button3,0,2);
    layout->addWidget(button4,1,0);
    layout->addWidget(button5,1,1);
    layout->addWidget(button6,1,2);

    this->setLayout(layout);

    //设置水平拉伸系数
    layout->setColumnStretch(0,3);
    layout->setColumnStretch(1,1);
    layout->setColumnStretch(2,3);//这三个列是按照3：1：3来设置的
//  layout->setColumnStretch(0,0)//当比例为0时，说明不参与比例进去。
}

Widget::~Widget()
{
    delete ui;
}

