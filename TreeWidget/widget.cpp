#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //设置一下根节点的名字
    ui->treeWidget->setHeaderLabel("动物");

    //新增顶层节点
    QTreeWidgetItem* item1 = new QTreeWidgetItem();
    //每个节点都可以设置多个
    item1->setText(0,"猫");
    //添加到顶层节点
    ui->treeWidget->addTopLevelItem(item1);

    //新增顶层节点
    QTreeWidgetItem* item2 = new QTreeWidgetItem();
    //每个节点都可以设置多个
    item2->setText(0,"狗");
    //添加到顶层节点
    ui->treeWidget->addTopLevelItem(item2);

    //新增顶层节点
    QTreeWidgetItem* item3 = new QTreeWidgetItem();
    //每个节点都可以设置多个
    item3->setText(0,"鸟");
    //添加到顶层节点
    ui->treeWidget->addTopLevelItem(item3);

    //创建子节点
    QTreeWidgetItem* item4 = new QTreeWidgetItem();
    item4->setText(0,"中华田园猫");
    item1->addChild(item4);

    QTreeWidgetItem* item5 = new QTreeWidgetItem();
    item5->setText(0,"布偶猫");
    item1->addChild(item5);

    QTreeWidgetItem* item6 = new QTreeWidgetItem();
    item6->setText(0,"暹罗猫");
    item1->addChild(item6);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_insertTopItem_clicked()
{
    //先获取到输入框内容
    const QString& text = ui->lineEdit->text();
    //构造一个item
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,text);
    //放到顶层元素
    ui->treeWidget->addTopLevelItem(item);
}

void Widget::on_pushButton_insertItem_clicked()
{
    //获取当前的节点
    QTreeWidgetItem* curItem = ui->treeWidget->currentItem();
    if(curItem==nullptr)
    {
        return;
    }
    //先获取到输入框内容
    const QString& text = ui->lineEdit->text();
    //构造一个item来放置存在输入框中的数据
    QTreeWidgetItem* item = new QTreeWidgetItem();
    item->setText(0,text);
   //插入到子节点
    curItem->addChild(item);

}

void Widget::on_pushButton_3_clicked()
{
    //获取选中的元素
    QTreeWidgetItem* curItem = ui->treeWidget->currentItem();
    if(curItem == nullptr)
    {
        return;
    }
    //删除选中的元素,还需要获取父元素，在进行删除
    QTreeWidgetItem* parent = curItem->parent();
    if(parent == nullptr)
    {
        //顶层元素
        //获取第几个顶层元素的下标，再进行删除
        int index = ui->treeWidget->indexOfTopLevelItem(curItem);
        ui->treeWidget->takeTopLevelItem(index);
    }
    else
    {
        //普通元素
        parent->removeChild(curItem);
    }
}
