#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    qDebug()<<"see the attribute";
}

void Widget::on_pushButton_enable_clicked()
{
    //切换按钮的禁用状态
    //先获取到一个按钮的使用状态
    bool enable = ui->pushButton->isEnabled();
    if(enable){
        ui->pushButton->setEnabled(false);
    }
    else{
        ui->pushButton->setEnabled(true);
    }

}
