#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //ui->pushButton是访问form file中创建的控件
    connect(ui->pushButton,&QPushButton::clicked,this,&Widget::handleClick);//连接(谁发出信号，发出的什么信号(点击按钮自动触发)，发送给谁处理，具体怎么处理)
}

Widget::~Widget()
{
    delete ui;
}
//当按钮被点击后，把按钮中的文本信息进行切换
void Widget::handleClick()
{
    if(ui->pushButton->text()==QString("hello world"))//->setText是编辑文本，->text是读取文本。
    {
        ui->pushButton->setText("hello qt");
    }
    else
    {
        ui->pushButton->setText("hello world");
    }
}

