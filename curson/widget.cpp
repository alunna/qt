#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //访问到图片
   QPixmap pixmap(":/chicken.png");
   pixmap = pixmap.scaled(100,200);
   //基于图片生成光标
   QCursor cursor(pixmap,10,10);
   //光标设置进去
   this->setCursor(cursor);
}

Widget::~Widget()
{
    delete ui;
}

