#include "widget.h"
#include "ui_widget.h"

#include <QTimer>
#include <QDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //设置初始值
    ui->lcdNumber->display("10");
    //先去创建一个Qtimer实例
    QTimer* timer = new QTimer(this);
    //把 Qtimer 的 timeout 信号和自己的槽函数进行连接
    connect(timer,&QTimer::timeout,this,&Widget::handle);
    //自动定时器,参数是触发 timeout的周期，单位是ms
    timer->start(1000);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::handle()
{
    //先拿到LCD中的数字
    int value = ui->lcdNumber->intValue();
    if(value<=0)//数字减到0，停止
    {
        timer->stop();
        return;
    }
    ui->lcdNumber->display(value-1);
}

