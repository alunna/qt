#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //初始化，设置为密码形式
    ui->lineEdit->setEchoMode(QLineEdit::Password);
    ui->lineEdit->setClearButtonEnabled(true);
    ui->lineEdit_2->setEchoMode(QLineEdit::Password);
    ui->lineEdit_2->setClearButtonEnabled(true);
    ui->label->setText("密码为空");
    //使用textEdited信号，来触发两个输入框的文件

}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_lineEdit_textEdited(const QString &arg1)
{
    (void) arg1;
   this->compare();
//    const QString& s1 = ui->lineEdit->text();
//    const QString& s2 = ui->lineEdit_2->text();

//    if(s1.isEmpty() && s2.isEmpty())
//    {
//        ui->label->setText("密码为空");
//    }
//    else if(s1== s2)
//    {
//       ui->label->setText("两次密码一致");
//    }
//    else
//    {
//        ui->label->setText("两次密码不一致!");
//    }
}

void Widget::on_lineEdit_2_textEdited(const QString &arg1)
{
    (void) arg1;
    this->compare();
//    const QString& s1 = ui->lineEdit->text();
//    const QString& s2 = ui->lineEdit_2->text();

//    if(s1.isEmpty() && s2.isEmpty())
//    {
//        ui->label->setText("密码为空");
//    }
//    else if(s1== s2)
//    {
//       ui->label->setText("两次密码一致");
//    }
//    else
//    {
//        ui->label->setText("两次密码不一致!");
//    }
}

void Widget::compare()
{
    const QString& s1 = ui->lineEdit->text();
    const QString& s2 = ui->lineEdit_2->text();

    if(s1.isEmpty() && s2.isEmpty())
    {
        ui->label->setText("密码为空");
    }
    else if(s1== s2)
    {
       ui->label->setText("两次密码一致");
    }
    else
    {
        ui->label->setText("两次密码不一致!");
    }
}
