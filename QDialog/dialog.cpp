#include "dialog.h"
#include "ui_dialog.h"
#include <QDialog>

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    ui->setupUi(this);

}

Dialog::~Dialog()
{
    delete ui;
}


void Dialog::on_pushButton_clicked()
{
    QDialog* diaLog = new QDialog(this);
    diaLog->setWindowTitle("阿里巴巴");//设置标题

    //设置大小
    diaLog->resize(400,400);

    //显示对话框
    diaLog->show();

    //delete dialog;
    //正确的做法是把delete 与关闭按钮的点击信号关联起来
    //Qt 中有一个属性，通过设置属性，直接完成
    diaLog->setAttribute(Qt::WA_DeleteOnClose);
}
