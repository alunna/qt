#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //1.创建菜单栏
    QMenuBar* menuBar = new QMenuBar();
    this->setMenuBar(menuBar);

    //2.创建菜单，并将菜单加入到菜单栏中去
    QMenu* menu1 = new QMenu("文件(&F)");
    QMenu* menu2 = new QMenu("编辑(&E)");
    QMenu* menu3 = new QMenu("构建(&B)");
    QMenu* menu4 = new QMenu("调试(&D)");
    QMenu* menu5 = new QMenu("工具(&T)");
    QMenu* menu6 = new QMenu("控件(&W)");

    menuBar->addMenu(menu1);
    menuBar->addMenu(menu2);
    menuBar->addMenu(menu3);
    menuBar->addMenu(menu4);
    menuBar->addMenu(menu5);
    menuBar->addMenu(menu6);

    //3.创建菜单项,并把菜单项放入到菜单中去
    QAction* action1 = new QAction("菜单项1");
    QAction* action2 = new QAction("菜单项2");
    QAction* action3 = new QAction("菜单项3");
    QAction* action4 = new QAction("菜单项4");
    QAction* action5 = new QAction("菜单项5");
    QAction* action6 = new QAction("菜单项6");

    menu1->addAction(action1);
    menu2->addAction(action2);
    menu3->addAction(action3);
    menu4->addAction(action4);
    menu5->addAction(action5);
    menu6->addAction(action6);


}

MainWindow::~MainWindow()
{
    delete ui;
}

