#include "widget.h"
#include "ui_widget.h"

#include <QPushButton>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    setWindowTitle("window 标题");

    QPushButton* button = new QPushButton(this);
    button->setText("按钮");
    button->setWindowTitle("new 窗口标题");
}

Widget::~Widget()
{
    delete ui;
}

