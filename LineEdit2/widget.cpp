#include "widget.h"
#include "ui_widget.h"

#include <QRegExpValidator>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //给单行输入框设置验证器，基于正则表达式来验证
    QRegExp regExp("^1\\d{10}$");
    ui->lineEdit->setValidator(new QRegExpValidator(regExp));//设置一个验证器

}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_lineEdit_textEdited(const QString &text)
{
    //验证通过，按钮可用，不通过，按钮不可用
    QString content = text;
    int pos = 0;
    if(ui->lineEdit->validator()->validate(content,pos)==QValidator::Acceptable){
        //验证通过
        ui->pushButton->setEnabled(true);
    }else{
        ui->pushButton->setEnabled(false);
    }
}
