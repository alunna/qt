#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT//如果让某个类可以使用信号槽，必须在类最开始的时候写下 Q_OBJECT 宏

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();
    signals:// Qt 自己拓展出来的关键字，qmake时调用一些代码的分析工具，会把下方声明的东西默认为信号。
        void mySignal(const QString&);
    //    void handleClicked();
    //private slots:
    //    void on_pushButton1_clicked();

public slots:
        void  handleMySignal(const QString&);

private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
