#include "widget.h"
#include "ui_widget.h"

#include <QPushButton>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(this,&Widget::mySignal,this,&Widget::handleMySignal);
    //发出自定义信号
    //可以在任意位置发送信号，不一定非要在这里发送
    emit mySignal("xinhao");
//    QPushButton* button = new QPushButton(this);
//    button->setText("按钮");
//    button->move(100,100);

//    connect(button,&QPushButton::clicked,this,&Widget::handleClicked);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::handleMySignal(const QString& text)
{
    this->setWindowTitle("发出信号");
}

//void Widget::handleClicked()
//{
//    //按下按钮修改窗口标题
//    this->setWindowTitle("按钮已经按下");
//}



//void Widget::on_pushButton1_clicked()
//{
//    this->setWindowTitle("按钮已经按下!");
//}

