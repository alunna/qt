#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //初始化列表
    //创建三行
    ui->tableWidget->insertRow(0);
    ui->tableWidget->insertRow(1);
    ui->tableWidget->insertRow(2);

    //创建三列
    ui->tableWidget->insertColumn(0);
    ui->tableWidget->insertColumn(1);
    ui->tableWidget->insertColumn(2);

    //给三个列设置水平方向表头
    ui->tableWidget->setHorizontalHeaderItem(0,new QTableWidgetItem("学号"));
    ui->tableWidget->setHorizontalHeaderItem(1,new QTableWidgetItem("姓名"));
    ui->tableWidget->setHorizontalHeaderItem(2,new QTableWidgetItem("年龄"));

    //给表格中添加数据
    ui->tableWidget->setItem(0,0,new QTableWidgetItem("100"));
    ui->tableWidget->setItem(0,1,new QTableWidgetItem("张三"));
    ui->tableWidget->setItem(0,2,new QTableWidgetItem("18"));

    ui->tableWidget->setItem(1,0,new QTableWidgetItem("101"));
    ui->tableWidget->setItem(1,1,new QTableWidgetItem("李四"));
    ui->tableWidget->setItem(1,2,new QTableWidgetItem("19"));

    ui->tableWidget->setItem(2,0,new QTableWidgetItem("103"));
    ui->tableWidget->setItem(2,1,new QTableWidgetItem("王五"));
    ui->tableWidget->setItem(2,2,new QTableWidgetItem("18"));
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_insertRow_clicked()
{
    //先知道几行
    int rowCount = ui->tableWidget->rowCount();
    //在最后一行增加一行
    ui->tableWidget->insertRow(rowCount);
}

void Widget::on_pushButton_deleteRow_clicked()
{
    //获取选中的行号
    int currow = ui->tableWidget->currentRow();
    //删除选中行
    ui->tableWidget->removeRow(currow);
}

void Widget::on_pushButton_insertColumn_clicked()
{
    //先获取几列
    int columnCount = ui->tableWidget->columnCount();
    //新增列数
    ui->tableWidget->insertColumn(columnCount);
    //设置列的头
    const QString& text = ui->lineEdit->text();
    ui->tableWidget->setHorizontalHeaderItem(columnCount, new QTableWidgetItem(text));
}

void Widget::on_pushButton_deleteColumn_clicked()
{
    //获取选中列
    int curcol = ui->tableWidget->currentColumn();
    //删除选中列
    ui->tableWidget->removeColumn(curcol);
}
