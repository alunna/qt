#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_textEdit_textChanged()
{
    //TextEdit的输入框中的内容实时传递到label中
    const QString text = ui->textEdit->toPlainText();
    ui->label->setText(text);
}
