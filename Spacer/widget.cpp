#include "widget.h"

#include "ui_widget.h"

#include <QPushButton>
#include <QHBoxLayout>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    QHBoxLayout* layout = new QHBoxLayout();
    this->setLayout(layout);

    QPushButton* button1 = new QPushButton("按钮1");
    QPushButton* button2 = new QPushButton("按钮2");

    //创建一个SPacer 使两个按钮之间存在空白
    QSpacerItem* spacer = new QSpacerItem(100,20);

    //是把两个按钮之间的空白放大
    layout->addSpacerItem(spacer);
    layout->addWidget(button1);
    layout->addSpacerItem(spacer);
    layout->addWidget(button2);


}

Widget::~Widget()
{
    delete ui;
}

