#include "label.h"
#include <QDebug>
#include <QMouseEvent>
Label::Label(QWidget* parent):QLabel(parent)
{

}

void Label::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        qDebug() <<  "按下左键";
    }
    else if(event->button() == Qt::RightButton)
    {
        qDebug() << "按下右键";
    }
    else if(event->button() == Qt::MidButton)
    {
        qDebug() << "按下中键";
    }
    //当前event对象就包含了鼠标点击位置的坐标
    //以Label框为的左上角为起始坐标
    //qDebug() << event->x() << "," << event->y();
    //以屏幕的左上角为起始坐标
    //需要注意的是，不仅仅鼠标左键可以作为点击按键，右键同样可以，侧键也可以。
    //所以event响应的是单纯的鼠标点击事件
    //我们想要去区分左右键还是需要进行判断
    //qDebug() << event->globalX() << "," << event->globalY();

}

void Label::mouseReleaseEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        qDebug() << "左键释放";
    }
    else if(event->button() == Qt::RightButton)
    {
        qDebug() << "右键释放";
    }
    else if(event-> button() == Qt::MidButton)
    {
        qDebug() << "中间释放";
    }
}

void Label::mouseDoubleClickEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        qDebug() << "双击左键";
    }
    else if(event->button() == Qt::RightButton)
    {
        qDebug() << "双击右键";
    }
}
