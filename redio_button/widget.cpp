#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //添加一个默认的选项为男
    ui->radioButton_male->setChecked(true);
    ui->label->setText("You choose your gender is male");
    //checkable 只是能够让按钮不被选定，仍是可以相应点击事件的
    ui->radioButton_other->setCheckable(false);//禁用其他选项
    ui->radioButton_other->setEnabled(false);//彻底禁用
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_radioButton_male_clicked()
{
    // 把界面上的label内容进行重写
    ui->label->setText("You choose your gender is male");
}

void Widget::on_radioButton_female_clicked()
{
    // 把界面上的label内容进行重写
    ui->label->setText("You choose your gender is famale");
}


void Widget::on_radioButton_other_clicked()
{
    // 把界面上的label内容进行重写
    ui->label->setText("You choose your gender is other");
}
