#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <fstream>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //需要读取文件内容，每一行读取出来作为Combobox选项
    std::ifstream file("C:/Users/Admin/Desktop/config.txt");
    if(!file.is_open()){
        qDebug()<<"文件打开失败";
        return;
    }
    //按行读取内容
    //使用getline来进行
    std::string line;
    while(std::getline(file,line))
    {
        //取到的每一行设置到下拉框
        ui->comboBox->addItem(QString::fromStdString(line));
    }
    file.close();
}

Widget::~Widget()
{
    delete ui;
}

