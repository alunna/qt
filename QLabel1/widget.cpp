#include "widget.h"
#include "ui_widget.h"
#include <QDebug>
#include <QResizeEvent>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //先把QLabel设置成和窗口一样大，并且把QLabel左上角设置到窗口的左上角
    //铺满整个窗口
    QRect windowRect = this->geometry();
    ui->label->setGeometry(0,0,windowRect.width(),windowRect.height());

    QPixmap pixmap(":/huaji.png");

    ui->label->setPixmap(pixmap);
    ui->label->setScaledContents(true);//拉伸填充满QLabel

//    //把第二个label设置成富文本
//    ui->label_2->setTextFormat(Qt::RichText);
//    ui->label_2->setText("<b>fu 文本</b>");

//    ui->label_3->setTextFormat(Qt::AutoText);
//    ui->label_3->setText("# Auto 文本");
}

Widget::~Widget()
{
    delete ui;
}
//此处的形参是包含了触发这个 resize 事件的这一时刻，窗口的尺寸数值
void Widget::resizeEvent(QResizeEvent *event)
{
    qDebug()<<event->size();
    ui->label->setGeometry(0,0,event->size().width(),event->size().height());
}

