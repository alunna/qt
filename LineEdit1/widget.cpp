#include "widget.h"
#include "ui_widget.h"
#include <QtDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //初始化第一个输入框
    ui->lineEdit_name->setPlaceholderText("请输入姓名");//提示信息
    ui->lineEdit_name->setClearButtonEnabled(true);//清空输入框

    //初始化第二个输入框 输入密码
    ui->lineEdit_password->setPlaceholderText("请输入密码");
    ui->lineEdit_password->setClearButtonEnabled(true);
    ui->lineEdit_password->setEchoMode(QLineEdit::Password);//把显示模式改为显示密码的模式

    //第三个电话输入框
    ui->lineEdit_phone->setPlaceholderText("请输入电话号码");
    ui->lineEdit_password->setClearButtonEnabled(true);
    //手机号码有固定个数,此处的0代表为 数字
    ui->lineEdit_phone->setInputMask("000-0000-0000");//InputMark可以对输入的内容做简单的校验
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    QString gender = ui->radioButton_male->isChecked()?"男":"女";

    qDebug()<< "姓名："<<ui->lineEdit_name->text()
            <<"密码："<<ui->lineEdit_password->text()
           <<"性别："<<gender
          <<"电话："<<ui->lineEdit_phone->text();
}
