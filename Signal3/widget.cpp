#include "widget.h"
#include "ui_widget.h"
#include <QPushButton>
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    QPushButton* button = new QPushButton(this);
    button->setText("按钮");
    button->move(200,200);
    connect(button,&QPushButton::clicked,this,[button,this](){
        qDebug()<<"lambda被执行了";
        qDebug()<<"hhhahahaahaa";

        button->move(300,300);
        this->move(400,100);
    });
}

Widget::~Widget()
{
    delete ui;
}

