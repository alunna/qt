#include "widget.h"
#include "ui_widget.h"
#include <QtDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_pushButton_clicked()
{
    //获取两个输入框的时间
    QDateTime timeOld = ui->dateTimeEdit->dateTime();
    QDateTime timeNew = ui->dateTimeEdit_2->dateTime();
    qDebug()<<timeOld <<timeNew;
    //计算日期的差值
    int day = timeOld.daysTo(timeNew);//对于隔天的差值，可能会多计算一天。所以我们从新用秒来计算
    int seconds = timeOld.secsTo(timeNew);

    //秒换成小时，查看零头是多少
    int hours = (seconds/3600) % 24;
    int days = (seconds/3600) % 60;
    qDebug()<<days<<"天";
    qDebug()<<hours<<"小时";
    qDebug()<<seconds<<"秒";
    //显示到label上面
    ui->label->setText(QString("已经持续了")+QString::number(day)+QString("天")+QString::number(hours)+QString("小时"));
}
