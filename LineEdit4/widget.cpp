#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    //初始化按照密码方式
    ui->lineEdit->setEchoMode(QLineEdit::Password);

}

Widget::~Widget()
{
    delete ui;
}


void Widget::on_checkBox_toggled(bool checked)
{
    if(checked)
    {
        //true 的话显示密码，输入框的模式为Normal
        ui->lineEdit->setEchoMode(QLineEdit::Normal);
    }
    else
    {
        //false的话是隐藏密码，把输入框的模式调整为Password
        ui->lineEdit->setEchoMode(QLineEdit::Password);
    }
}
