#include "widget.h"
#include "ui_widget.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    //创建垂直管理器中
    QVBoxLayout* vlay = new QVBoxLayout();
    this->setLayout(vlay);

    //创建几个按钮放进去
    QPushButton* button1 = new QPushButton("按钮1");
    QPushButton* button2 = new QPushButton("按钮2");
    vlay->addWidget(button1);
    vlay->addWidget(button2);

    //创建水平管理器
    QHBoxLayout* hlay = new QHBoxLayout();
    this->setLayout(hlay);

    //创建按钮放进去
    QPushButton* button3 = new QPushButton("按钮3");
    QPushButton* button4 = new QPushButton("按钮4");
    hlay->addWidget(button3);
    hlay->addWidget(button4);

    //把水平管理器放到垂直管理器中
    vlay->addLayout(hlay);
}

Widget::~Widget()
{
    delete ui;
}

