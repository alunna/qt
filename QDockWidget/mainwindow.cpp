#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDockWidget>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //给主窗口添加一个子窗口
    QDockWidget* dockWidget = new QDockWidget();
    this->addDockWidget(Qt::LeftDockWidgetArea,dockWidget);//两个参数，一个是放置位置，一个是放置东西

    //浮动窗口设置标题
    dockWidget->setWindowTitle("这是浮动窗口");

    //给浮动窗口内部增加控件
    //不能直接添加，二十需要添加一个QWidget，把需要添加的控件加入到QWidget中
    //然后把QWidget 设置到 dockWidget
    QWidget* container = new QWidget();
    dockWidget->setWidget(container);

    //创建布局管理器，把管理器设置到Qwidget
    QVBoxLayout* layout = new QVBoxLayout;
    container->setLayout(layout);

    //控件放到layout中
    QLabel* label = new QLabel("这是一个标签");
    layout->addWidget(label);
    QPushButton* button = new QPushButton("按钮");
    layout->addWidget(button);

    //设置浮动窗口允许存在的位置
    dockWidget->setAllowedAreas(Qt::LeftDockWidgetArea | Qt:: TopDockWidgetArea);

}
MainWindow::~MainWindow()
{
    delete ui;
}

