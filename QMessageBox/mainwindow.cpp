#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QPushButton>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    QMessageBox* messBox = new QMessageBox(this);
    messBox->setWindowTitle("对话框标题");
    messBox->setText("这是一个对话框的文本");
    messBox->setIcon(QMessageBox::Warning);//内置警报图标
    //在标准按钮中，根本无法连接到信号槽（信号槽是QMessageBox自己生成的）
    messBox->setStandardButtons(QMessageBox::Ok | QMessageBox::Save | QMessageBox::Cancel);//内置的标准按钮

//    QPushButton* button = new QPushButton("按钮",messBox);
//    messBox->addButton(button,QMessageBox::AcceptRole);

    //connect连接信号槽，来进行一些相关操作



    //非模态弹框,但是使用场景更多的是模态弹窗
    //messBox->show();
    //弹出模态对话框，当对话框处于弹出状态时后，代码机会在 exec 这里阻塞，一直到对话框关闭
    //当用户点击按钮，当对话框关闭后，就可以通过exec 的返回值，来知道用户点击的是那个按钮，从而执行一些相关的逻辑
    int result = messBox->exec();//返回的是枚举的值
    if(result == QMessageBox::Ok){
        qDebug()<<"Ok";
    }else if(result == QMessageBox::Save){
        qDebug()<<"Save";
    }else if(result == QMessageBox::Cancel){
        qDebug()<<"Cancle";
    }

    messBox->setAttribute(Qt::WA_DeleteOnClose);
    //delete messBox;
}
